<?
class __Debug
  {const SHOW_ERRORS = false;
   protected static $is_debugger = NULL;
   protected static $is_new = NULL;
   protected static $is_test = NULL;
   protected static $inited = NULL;
   protected static $showErrors = NULL;
   public static function isOn()
     {static::init();
      return !empty(static::$is_debugger);
     }
   public static function showErrors($show = NULL)
     {if(!is_null($show))
        {if(static::$showErrors = $show)
           {ini_set('display_errors', true);
            error_reporting(E_ALL);              
           }
         else
           {ini_set('display_errors', false);
            error_reporting(0);              
           }
        }
      return static::$showErrors;
     }
   public static function isTest()
     {return in_array($_SERVER['HTTP_HOST'], ['devilscup.ru']);
      static::init();
      return !static::isNew() && !empty(static::$is_test);
     }
   public static function isNew()
     {static::init();
      return !empty(static::$is_new);
     }
   public static function init()
     {if(is_null(static::$inited))
        {static::$inited = false;
         if(isset($_GET['is_debugger']))
           {setcookie('is_debugger', (static::$is_debugger = ((boolean) ((int) $_GET['is_debugger']))) ? 1 : 0, (time() + 3600), '/');
           }
         elseif(is_null(static::$is_debugger))
           {static::$is_debugger = array_key_exists('is_debugger', $_COOKIE) ? ((boolean) ((int) $_COOKIE['is_debugger'])) : false;
           }
         if(static::$is_debugger && static::SHOW_ERRORS)
           {error_reporting(E_ALL);
            ini_set('display_errors', true);
           }
         if(isset($_GET['is_new']) || array_key_exists('REMOTE_USER', $_SERVER) && in_array($_SERVER['REMOTE_USER'], ['new-tester']))
           {setcookie('is_new', static::$is_new = (in_array($_SERVER['REMOTE_USER'], ['new-tester']) || (static::$is_new = ((boolean) ((int) $_GET['is_new'])))) ? 1 : 0, (time() + 3600), '/');
           }
         elseif(is_null(static::$is_new))
           {static::$is_new = array_key_exists('is_new', $_COOKIE) ? ((boolean) ((int) $_COOKIE['is_new'])) : false;
           }
         if(isset($_GET['is_test']) || in_array($_SERVER['HTTP_HOST'], ['devilscup.ru']))
           {setcookie('is_test', static::$is_test = (in_array($_SERVER['HTTP_HOST'], ['devilscup.ru']) || (static::$is_test = ((boolean) ((int) $_GET['is_test'])))) ? 1 : 0, (time() + 60 * 60 * 24 * 365 * 1000), '/');
           }
         elseif(is_null(static::$is_test))
           {static::$is_test = array_key_exists('is_test', $_COOKIE) ? ((boolean) ((int) $_COOKIE['is_test'])) : false;
           }
         static::$inited = true;
        }
     }
   public static function getInfo($item, $type = NULL)
     {if(is_string($item) || (is_array($item) && count($item) == 2))
        {$reflectionObject = NULL;
         if(class_exists($item))
           {$reflectionObject = new ReflectionClass($item);
           }
         elseif(is_callable($item))
           {$reflectionObject = is_array($item) ? new ReflectionMethod($item[0], $item[1]) : new ReflectionFunction($item);
           }           
        }
      else
        {if(is_null($type))
           {$type = is_callable($item) ? 'function' : 'class';
           }
         $className = 'Reflection'.ucfirst($type);
         $reflectionObject = new $className($item);
        }
      return isset($reflectionObject) ? $reflectionObject->getFileName() : 'Unknown'.(isset($type) ? " type" : '');
     }
   public static function out($data, $die = true)
     {if(static::isOn())
        {echo '<pre>';
         var_dump($data);
         /*
         is_string($data) ? print $data : var_dump($data);
         */
         if($die)
           {debug_print_backtrace();
            die('</pre>');
           }
         echo '</pre>';
        }
     }
   public static function outInfo($data, $type = NULL, $die = true)
     {return static::out(static::getInfo($data, $type), $die);
     }
   public static function logInfo($data, $type = NULL)
     {return static::log(static::getInfo($data, $type));
     }
   public static function log($givenText)
     {$text = file_exists($filePath = "${_SERVER['DOCUMENT_ROOT']}/log.txt") ? file_get_contents($filePath) : '';
      if($text)
        {$text .= "\r\n";
        }
      if(is_array($givenText))
        {$givenText = var_export($givenText, true);           
        }
      $text .= implode(':', [date('Y-m-d H:i:s'), $givenText]);
      file_put_contents($filePath, $text);
     }
   
  }
__Debug::init();