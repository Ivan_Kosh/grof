<?php
// Heading
$_['heading_title']    = 'Корзина покупок';

// Text
$_['text_success']     = '<DIV class = "shadow"></DIV><DIV class = "window credit"><DIV class = "icon close" onclick = "$(\'.shadow, .window\').remove();"></DIV><DIV class = "content"><DIV class = "main"><DIV class = "success icon"></DIV><DIV class = "title">Заявка на звонок менеджера по кредитам</DIV></DIV><DIV class = "details"><DIV class = "product"><IMG class = "image" src = "%s"></IMG><DIV class = "properties"><DIV class = "title">%s</DIV><DIV class = "article">Артикул: %s</DIV><DIV class = "price">%s</DIV></DIV></DIV></DIV><DIV class = "form"><DIV class = "content"><DIV class = "element"><DIV class = "title">ФИО<SPAN class = "required">*</SPAN></DIV><INPUT class = "full-name"></INPUT></DIV><DIV class = "element"><DIV class = "title">Телефон<SPAN class = "required">*</SPAN></DIV><INPUT class = "phone"></INPUT></DIV></DIV><DIV class = "buttons"><BUTTON class = "to-checkout" onclick = "$.post(\'/index.php?route=checkout/credit/send\', {message: $(\'.window > .content > .details\').html(), \'full-name\': $(\'.element > .full-name\').val(), \'phone\': $(\'.element > .phone\').val()}, function()
    {alert(\'Заявка успешно отправлена\'); $(\'.shadow, .window\').remove();
    });">Оформить заказ</BUTTON><BUTTON class = "to-continue" onclick = "$(\'.shadow, .window\').remove();">Продолжить покупки</BUTTON></DIV></DIV></DIV><!--<a href="%s">%s</a> добавлен <a href="%s">в корзину покупок</a>!--></DIV><SCRIPT>$(\'.window input.phone\').mask(\'+7 (000) 000 - 00 - 00\', {placeholder: \'+7 (___) ___ - __ - __\'})</SCRIPT>';
$_['text_remove']      = 'Корзина покупок изменена!';
$_['text_login']       = 'Необходимо <a href="%s">авторизироваться</a> или <a href="%s">создать учетную запись</a> для просмотра цен!';
$_['text_items']       = 'Товаров %s  (%s)';
$_['text_points']      = 'Бонусные баллы: %s';
$_['text_next']        = 'Что бы вы хотели сделать дальше?';
$_['text_next_choice'] = 'Если у вас есть код купона на скидку или бонусные баллы, которые вы хотите использовать, выберите соответствующий пункт ниже. А также, Вы можете приблизительно узнать стоимость доставки в ваш регион.';
$_['text_empty']       = 'Корзина пуста!';
$_['text_day']         = 'день';
$_['text_week']        = 'неделю';
$_['text_semi_month']  = 'полмесяца';
$_['text_month']       = 'месяц';
$_['text_year']        = 'год';
$_['text_trial']       = 'Стоимость: %s; Периодичность: %s %s; Кол-во платежей: %s; Далее, ';
$_['text_recurring']   = 'Стоимость: %s; Периодичность: %s %s';
$_['text_length']      = ' Кол-во платежей: %s';
$_['text_until_cancelled']   	= 'до отмены';
$_['text_recurring_item']    	              = 'Периодические платежи';
$_['text_payment_recurring']                    = 'Платежный профиль';
$_['text_trial_description'] 	              = 'Стоимость: %s; Периодичность: %d %s; Кол-во платежей: %d;  Далее,  ';
$_['text_payment_description'] 	              = 'Стоимость: %s; Периодичность: %d %s; Кол-во платежей: %d';
$_['text_payment_until_canceled_description'] = 'Стоимость: %s; Периодичность: %d %s; Кол-во платежей: до отмены';

// Column
$_['column_image']          = 'Изображение';
$_['column_name']           = 'Название';
$_['column_model']          = 'Модель';
$_['column_quantity']       = 'Количество';
$_['column_price']          = 'Цена за шт.';
$_['column_total']          = 'Всего';

// Error
$_['error_stock']            = 'Товары отмеченные *** отсутствуют в нужном количестве или их нет на складе!';
$_['error_minimum']          = 'Минимальное количество для заказа товара %s составляет %s!';
$_['error_required']         = '%s обязательно!';
$_['error_product']          = 'В вашей корзине нет товаров!';
$_['error_recurring_required'] = 'Пожалуйста, выберите периодичность платежа!';
