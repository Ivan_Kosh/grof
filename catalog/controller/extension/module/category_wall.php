<?php
class ControllerExtensionModuleCategoryWall extends Controller {
	public function index($setting) {
		static $module = 0;
		
		$this->load->language('extension/module/category_wall');

		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		
		$data['heading_titles'] = $setting['description_title'][$this->config->get('config_language_id')]['title'] ? $setting['description_title'][$this->config->get('config_language_id')]['title'] : $this->language->get('heading_titles');
		
		if ($setting['type'] == 1) {
			if (isset($this->request->get['path'])) {
				$parts = explode('_', (string)$this->request->get['path']);
			} else {
				$parts = array();
			}
			if (isset($parts[0])) {
				$data['category_id'] = $parts[0];
			} else {
				$data['category_id'] = 0;
			}
			if (isset($parts[1])) {
				$data['child_id'] = $parts[1];
			} else {
				$data['child_id'] = 0;
			}
		}

		if (!$setting['style']) {
			$data['style'] = 'product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12';
		} else {
			$data['style'] = $setting['style'];
		}
		
		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		if ($setting['type'] == 0) {
			if (!empty($setting['category'])) {
				
				$data['categories'] = array();
				$data['categories'] = $this->cache->get('category_wall.' . $this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'));
				
				if(!$data['categories']){
					$categories = array_slice($setting['category'], 0, (int)$setting['limit']);

					foreach ($categories as $category_id) {
						$category_info = $this->model_catalog_category->getCategory($category_id);

						if ($category_info) {
							

							if ($setting['subcategory']) {

								$children_data = array(); 

								$children = $this->model_catalog_category->getCategories($category_info['category_id']);

								foreach($children as $child) {
									$filter_data = array('filter_category_id' => $child['category_id'], 'filter_sub_category' => true);
									
									$filter_sub = array(
										'filter_category_id' => $child['category_id'],
										'filter_sub_category' => true
									);
									
									$total_subcat = $this->model_catalog_product->getTotalProducts($filter_sub);

									if($setting['hide_cat'] == 1) {
										if($total_subcat > 0)
											
										$children_data[] = array(
											'category_id' => $child['category_id'],
											'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
											'href' => $this->url->link('product/category', 'path=' . $category_info['category_id'] . '_' . $child['category_id'])
										);
									} else {
										$children_data[] = array(
											'category_id' => $child['category_id'],
											'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
											'href' => $this->url->link('product/category', 'path=' . $category_info['category_id'] . '_' . $child['category_id'])
										);
									}
								}
							} else {
								$children_data = false;
							}	
								
							if ($category_info['image']) {
								$image = $this->model_tool_image->resize($category_info['image'], $setting['width'], $setting['height']);
							} else {
								$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
							}		
							if ($setting['description']) {
								if ($category_info['description']) {
									$description = utf8_substr(strip_tags(html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8')), 0, $setting['description_limit']) . '..';
								} else {
									$description = false;
								}
							} else {
								$description = false;
							}	
							
							$filter_data = array(
								'filter_category_id'  => $category_info['category_id'],
								'filter_sub_category' => true
							);
							
							$data['categories'][] = array(
								'category_id' => $category_info['category_id'],
								'thumb'       => $image,
								'children'    => $children_data,
								'name'        => $category_info['name'],
								'description' => $description,
								'href'        => $this->url->link('product/category', 'path=' . $category_info['category_id'])
							);
						}
					}
					if($data['categories']){
						$this->cache->set('category_wall.' . $this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'), $data['categories']);
					}
				}
			}
		} elseif ($setting['type'] == 1) {
			$categori = $this->model_catalog_category->getCategories(0);
			
			$data['categories'] = array();
			
			$data['categories'] = $this->cache->get('category_walls.' . $this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'));
				
			if(!$data['categories']){
				$categories = array_slice($categori, 0, (int)$setting['limit']);

				foreach ($categories as $category) {
					if ($setting['subcategory']) {

						$children_data = array();

						$children = $this->model_catalog_category->getCategories($category['category_id']);

						foreach($children as $child) {
							$filter_data = array('filter_category_id' => $child['category_id'], 'filter_sub_category' => true);

							$filter_sub = array(
								'filter_category_id' => $child['category_id'],
								'filter_sub_category' => true
							);
							
							$total_subcat = $this->model_catalog_product->getTotalProducts($filter_sub);

							if($setting['hide_cat'] == 1) {
								if($total_subcat > 0)
									
								$children_data[] = array(
									'category_id' => $child['category_id'],
									'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
									'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
								);
							} else {
								$children_data[] = array(
									'category_id' => $child['category_id'],
									'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
									'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
								);
							}
						}
					} else {
						$children_data = false;
					}	
						
					if ($category['image']) {
						$image = $this->model_tool_image->resize($category['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}
					
					if ($setting['description']) {
						if ($category['description']) {
							$description = utf8_substr(strip_tags(html_entity_decode($category['description'], ENT_QUOTES, 'UTF-8')), 0, $setting['description_limit']) . '..';
						} else {
							$description = false;
						}
					} else {
						$description = false;
					}	
					
					$filter_data = array(
						'filter_category_id'  => $category['category_id'],
						'filter_sub_category' => true
					);
					
					$total_cat = $this->model_catalog_product->getTotalProducts($filter_data);

					if($setting['hide_cat'] == 1) {
						if($total_cat > 0)
						$data['categories'][] = array(
							'category_id' => $category['category_id'],
							'thumb'       => $image,
							'children'    => $children_data,
							'name'        => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
							'description' => $description,
							'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
						);
					} else {
						$data['categories'][] = array(
							'category_id' => $category['category_id'],
							'thumb'       => $image,
							'children'    => $children_data,
							'name'        => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
							'description' => $description,
							'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
						);
					}
				}
				if($data['categories']){
					$this->cache->set('category_walls.' . $this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id'), $data['categories']);
				}
			}
		}
		
		$data['module'] = $module++;
		
		if ($data['categories']) {
			return $this->load->view('extension/module/category_wall', $data);
		}
	}
}