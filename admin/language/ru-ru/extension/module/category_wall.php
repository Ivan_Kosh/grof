<?php
// Heading
$_['heading_title']    			= 'Category Wall - Стена категорий';

// Text
$_['text_extension']   			= 'Расширения';
$_['text_success']     			= 'Настройки успешно изменены!';
$_['text_edit']        			= 'Настройки модуля';
$_['text_all']        			= 'Все категории';
$_['text_select']        		= 'Выбранные категории';

// Entry
$_['entry_name']       			= 'Название модуля';
$_['entry_title']       		= 'Отображение заголовка';
$_['entry_category']  			= 'Категории';
$_['entry_subcategory']			= 'Подкатегории';
$_['entry_type']				= 'Тип';
$_['entry_description']			= 'Описание';
$_['entry_style']				= 'Css Стиль';
$_['entry_limit']      			= 'Лимит';
$_['entry_width']      			= 'Ширина';
$_['entry_height']     			= 'Высота';
$_['entry_status']     			= 'Статус';
$_['entry_hide_cat']  			= 'Скрыть пустые подкатегории';
$_['entry_description_limit']	= 'Лимит описания';

// Help
$_['help_category']     		= '(Автодополнение)';

// Error
$_['error_permission'] 			= 'У Вас нет прав для управления данным модулем!';
$_['error_name']       			= 'Название модуля должно содержать от 3 до 64 символов!';
$_['error_width']      			= 'Введите ширину изображения!';
$_['error_height']     			= 'Введите высоту изображения!';