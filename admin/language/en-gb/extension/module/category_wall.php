<?php
// Heading
$_['heading_title']    			= 'Category Wall';

// Text
$_['text_extension']   			= 'Extensions';
$_['text_success']    			= 'Success: You have modified featured module!';
$_['text_edit']        			= 'Edit Featured Module';
$_['text_all']        			= 'All category';
$_['text_select']        		= 'Select Category';

// Entry
$_['entry_name']       			= 'Module Name';
$_['entry_title']       		= 'Module Title';
$_['entry_category']  			= 'Category';
$_['entry_subcategory']			= 'Subcategory';
$_['entry_type']				= 'Type';
$_['entry_description']			= 'Description';
$_['entry_style']				= 'Css Style';
$_['entry_hide_cat']  			= 'Hide empty subcategory';
$_['entry_limit']      			= 'Limit';
$_['entry_width']      			= 'Width';
$_['entry_height']     			= 'Height';
$_['entry_status']     			= 'Status';
$_['entry_description_limit']	= 'limited';

// Help
$_['help_product']     			= '(Autocomplete)';

// Error
$_['error_permission'] 			= 'Warning: You do not have permission to modify featured module!';
$_['error_name']       			= 'Module Name must be between 3 and 64 characters!';
$_['error_width']      			= 'Width required!';
$_['error_height']     			= 'Height required!';