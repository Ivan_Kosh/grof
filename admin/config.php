<?php
// HTTP
define('HTTP_SERVER', 'http://grof.loc/admin/');
define('HTTP_CATALOG', 'http://grof.loc/');

// HTTPS
define('HTTPS_SERVER', 'http://grof.loc/admin/');
define('HTTPS_CATALOG', 'http://grof.loc/');

// DIR
define('DIR_APPLICATION', 'C:/Users/Professional/Downloads/OpenServer/domains/grof/admin/');
define('DIR_SYSTEM', 'C:/Users/Professional/Downloads/OpenServer/domains/grof/system/');
define('DIR_IMAGE', 'C:/Users/Professional/Downloads/OpenServer/domains/grof/image/');
define('DIR_STORAGE', 'C:/Users/Professional/Downloads/OpenServer/domains/storage/');
define('DIR_CATALOG', 'C:/Users/Professional/Downloads/OpenServer/domains/grof/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'grof');
define('DB_PASSWORD', 'grof');
define('DB_DATABASE', 'grof');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
